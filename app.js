const express = require("express");
const app = express();
const path = require("path");
const hbs = require("hbs");
const port = 4040;

const staticPath = path.join(__dirname,"/public");
const viewsPath = path.join(__dirname,"/views");

app.set("view engine", "hbs");
app.set("views", viewsPath);

app.use(express.static(staticPath));

app.get("/", (req,res) => {
    res.render("index");
})

app.listen(port, () => {
    console.log("App is running at port ",port);
})