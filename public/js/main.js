function initMap() {
    // Map options
    var options = {
      zoom: 13,
      center: { lat: 22.5823, lng: 88.4156 }
    }

    // New map
    var map = new google.maps.Map(document.getElementById('map'), options);

    // Array of markers
    var markers = [
      {
        coords: { lat: 22.581517, lng: 88.447535 },
        content: '<h1>Coal India</h1>'
      },
      {
        coords: { lat: 22.581530, lng: 88.450708 },
        content: '<h1>DLF-I (near subway)</h1>'
      },
      {
        coords: { lat: 22.581192, lng : 88.450641 },
        content: '<h1>DLF-I Opposite</h1>'
      },
      {
        coords: { lat: 22.582015, lng: 88.452773 },
        content: '<h1>Nazrul Tirtha(Newtown)</h1>'
      },
      {
        coords: { lat: 22.580877, lng: 88.462369 },
        content: '<h1>Novotel</h1>'
      },
      {
        coords: { lat: 22.579572, lng: 88.467103 },
        content: '<h1>Swapnabhor</h1>'
      },
      {
        coords: { lat: 22.579362, lng : 88.471104 },
        content: '<h1>Rabindra Tirtha-I</h1>'
      },
      {
        coords: { lat: 22.599931, lng : 88.471030 },
        content: '<h1>Prakriti Tirtha-I</h1>'
      },
      {
        coords: { lat: 22.607913, lng : 88.468305 },
        content: '<h1>Misti Hub</h1>'
      },
      {
        coords: { lat: 22.618646, lng : 88.464232 },
        content: '<h1>Akankha</h1>'
      },
      {
        coords: { lat: 22.621394, lng : 88.450863 },
        content: '<h1>City Centre-II</h1>'
      },
      {
        coords: { lat: 22.621083, lng : 88.453691 },
        content: '<h1>7th Rotary</h1>'
      },
      {
        coords: { lat: 22.618314, lng : 88.465259 },
        content: '<h1>Akankha</h1>'
      },
      {
        coords: { lat: 22.607132, lng : 88.468788 },
        content: '<h1>Misti Hub</h1>'
      },
      {
        coords: { lat: 22.604712, lng : 88.469883 },
        content: '<h1>Owl More</h1>'
      },
      {
        coords: { lat: 22.600712, lng : 88.470973 },
        content: '<h1>Prakriti Tirtha-2</h1>'
      },
      {
        coords: { lat: 22.592894, lng : 88.473469 },
        content: '<h1>westin</h1>'
      },
      {
        coords: { lat: +22.578826, lng : 88.472665 },
        content: '<h1>Rabindra Tirtha</h1>'
      },
      {
        coords: { lat: 22.577020, lng : 88.478696 },
        content: '<h1>Tata Cancer</h1>'
      },
      {
        coords: { lat: 22.580851, lng : 88.487201 },
        content: '<h1>TCS</h1>'
      },
      {
        coords: { lat: 22.588642, lng : 88.484410 },
        content: '<h1>ALIA West</h1>'
      },
      {
        coords: { lat: 22.588387, lng : 88.486251 },
        content: '<h1>ALIA East</h1>'
      },
      {
        coords: { lat: 22.580597, lng : 88.487269 },
        content: '<h1>TCS Opposite</h1>'
      },
      {
        coords: { lat: 22.573189, lng : 88.480168 },
        content: '<h1>Unitech Gate-1</h1>'
      },
      {
        coords: { lat: 22.574276, lng : 88.480192 },
        content: '<h1>Eye Hospital</h1>'
      },
      {
        coords: { lat: 22.576236, lng : 88.479271 },
        content: '<h1>Newtown Police Station</h1>'
      },
      {
        coords: { lat: 22.577836, lng : 88.475431 },
        content: '<h1>Techno India College</h1>'
      },
      {
        coords: { lat: 22.578748, lng : 88.471403 },
        content: '<h1>Rabindra Tirtha-2</h1>'
      },
      {
        coords: { lat: 22.581135, lng : 88.452895 },
        content: '<h1>1st Rotary</h1>'
      },
    ];

    // Loop through markers
    for (var i = 0; i < markers.length; i++) {
      // Add marker
      addMarker(markers[i]);
    }

    // Add Marker Function
    function addMarker(props) {
      var marker = new google.maps.Marker({
        position: props.coords,
        map: map,
        //icon:props.iconImage
      });

      // Check for customicon
      if (props.iconImage) {
        // Set icon image
        marker.setIcon(props.iconImage);
      }

      // Check content
      if (props.content) {
        var infoWindow = new google.maps.InfoWindow({
          content: props.content
        });

        marker.addListener('click', function () {
          infoWindow.open(map, marker);
        });
      }
    }
  }